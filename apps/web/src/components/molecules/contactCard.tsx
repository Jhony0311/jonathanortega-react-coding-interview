import { Box, Typography, Avatar } from '@mui/material';
import { SystemStyleObject, Theme } from '@mui/system';

import { Card } from '@components/atoms';
import { IContact } from 'react-coding-interview-shared/models';
import { InlineInput } from '@components/atoms/inlineInput';

export interface IContactCardProps {
  person: IContact;
  sx?: SystemStyleObject<Theme>;
}

export const ContactCard: React.FC<IContactCardProps> = ({
  person: { name, email },
  sx,
}) => {
  function onSubmit(fieldName: keyof IContact, value: unknown) {
    console.log(`Updating ${fieldName} to ${String(value)} on server`);
  }
  return (
    <Card sx={sx}>
      <Box display="flex" flexDirection="column" alignItems="center">
        <Avatar />
        <Box textAlign="center" mt={2}>
          <InlineInput value={name} onSubmit={(v) => onSubmit('name', v)}>
            <Typography variant="subtitle1" lineHeight="1rem">
              {name}
            </Typography>
          </InlineInput>
          <InlineInput value={email} onSubmit={(v) => onSubmit('email', v)}>
            <Typography variant="caption" color="text.secondary">
              {email}
            </Typography>
          </InlineInput>
        </Box>
      </Box>
    </Card>
  );
};
