// import React from 'react'

import { useState } from 'react';
import { TextField, IconButton } from '@mui/material';
import CheckIcon from '@mui/icons-material/Check';
import CloseIcon from '@mui/icons-material/Close';

import type { ReactNode } from 'react';
import type { TextFieldProps } from '@mui/material';

export type InlineInputProps = {
  onSubmit: (v: unknown) => void;
  children: ReactNode;
  validator?: (v: unknown) => boolean;
} & Pick<TextFieldProps, 'value' | 'type'>;

export function InlineInput({ value, type, onSubmit, children, validator }: InlineInputProps) {
  const [isEditing, setIsEditing] = useState(false);
  const [internalValue, setInternalValue] = useState(value);
  const [hasError, setHasError] = useState(false);

  function cancelEdit() {
    setInternalValue(value);
    setIsEditing(false);
  }

  function submitEdit() {
    onSubmit(internalValue);
    setIsEditing(false);
  }

  function handleChangeValue(v: unknown) {
    if(validator) {
      const isValid = validator(v);
      setHasError(!isValid);
    }
    setInternalValue(v);
  }

  if (isEditing) {
    return (
      <>
        <TextField
          error={hasError}
          type={type}
          value={internalValue}
          onChange={(e) => handleChangeValue(e.target.value)}
        />
        <IconButton onClick={cancelEdit}>
          <CloseIcon />
        </IconButton>
        <IconButton onClick={submitEdit}>
          <CheckIcon />
        </IconButton>
      </>
    );
  }
  // Revisar type de value
  return <div onClick={() => setIsEditing(true)}>
    {children}
  </div>;
}
