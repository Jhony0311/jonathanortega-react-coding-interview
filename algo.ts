function pairSum(arr: Array<number>, target: number): Array<number> {
  let a: number, b: number;

  arr.forEach((num1, i, z) => {
    z.forEach((num2, j) => {
      if (i !== j) {
        const t = num1 + num2 === target;
        if (t) {
          a = i;
          b = j;
        }
      }
    });
  });

  if (!a || !b) {
    throw new Error('No combination possible');
  }

  return [a, b];
}
